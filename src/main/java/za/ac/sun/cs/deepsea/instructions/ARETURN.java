package za.ac.sun.cs.deepsea.instructions;

import za.ac.sun.cs.deepsea.diver.Stepper;

/**
 * UNIMPLEMENTED &amp; BROKEN
 * 
 * @author Jaco Geldenhuys (geld@sun.ac.za)
 */
public class ARETURN extends Instruction {

	public ARETURN(Stepper stepper, int position) {
		super(stepper, position, 176);
	}
	
}
